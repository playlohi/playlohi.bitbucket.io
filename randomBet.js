let rndGens = null;
let rndNum = Math.random();
let rndSeed = rndNum.toFixed(52).replace('0.', '');
function rand(forceRandomize) {
  if (!rndGens) {
    rndGens = [
      PRNG.MRG32k3a,
      PRNG.Xorshift03,
      PRNG.KISS07,
      PRNG.LFib,
      PRNG.LFib4,
      PRNG.Alea,
      PRNG.Kybos,
      PRNG.Invwk
    ];
  }
  if (forceRandomize)
    forceRandomize = Math.floor(100 * Math.random() * forceRandomize);
  forceRandomize++;
  while (forceRandomize--) {
    rndNum = rndGens[Math.floor(rndNum * rndGens.length)](rndSeed)();
    rndSeed = (rndNum.toFixed(52).replace('0.', ''));
  }
  return rndNum;
}

$(document).ready(function () {
  initControls();
  $('#labelVersion').html('(v.1.2.7)');
});

let statConcurrency = {};
let statDiscrepancy = {};
let maxConcurency = 50;
for (let i=0;i<=maxConcurency;i++) {
  statConcurrency[i] = 0;
  statDiscrepancy[i] = 0;
}


let vMinimalProfitToBetReset = 0;
let vMaximalAllowableBet = 0;
let isAutoBetBullishMode = false;

function fitInputWidth(evt,el) {
  if (!el)
    el = $(evt.target);
  setTimeout(function () {
    let w = el[0].scrollWidth + 2;
    el.css('min-width', w + 'px').css('max-width', w + 'px');
  },250);
}

let chartBalance = null;
let chartBalanceData = [];
let elBetsTable = null;
let elTheBalance = null;
let nonce = 0;
function initControls() {
  elTheBalance = $('#inpTheBalance');
  elBetsTable = $('#betsTable');
  let elButtonResetBalance = $('#btResetBalance');
  let resetTimer = null;
  let rfc = $('#resetFactorContainer');
  function resetBalance() {
    if (isAutoBetRunning)
      return;
    let isReseted = false;
    if (elButtonResetBalance.hasClass('btn-dark')) {
      elButtonResetBalance.removeClass('btn-dark visually-hidden').addClass('btn-warning');
      $('#labelClickTrice').removeClass('visually-hidden');
      rfc.removeClass('visually-hidden');
    } else
    if (elButtonResetBalance.hasClass('btn-warning')) {
      elButtonResetBalance.removeClass('btn-warning').addClass('btn-danger');
    } else
    if (elButtonResetBalance.hasClass('btn-danger')) {
      elButtonResetBalance.removeClass('btn-danger').addClass('btn-dark');
      let newval = 1/10**(6-parseInt($('#inpResetFactor').val()));
      elTheBalance.val(newval.toFixed(8));
      elBetsTable.empty();
      betAmountMin();
      localStorage['balance'] = elTheBalance.val();
      localStorage['nonce'] = nonce = 0;
      localStorage['wagered'] = fStatWagered = 0;
      $('#statWagered').val(fStatWagered.toFixed(8));
      //$('#textareaLog').val('');
      statMaxGameBet = 0;
      fitInputWidth(0,elTheBalance);
      var bcw = parseInt($inpBalanceChartWidth.val())*33;
      var y = parseFloat(elTheBalance.val());
      chartBalanceData = [];
      for (var i=1; i<=bcw; i++)
        chartBalanceData.push({x: nonce-bcw+i, y: y});
      chartBalance.data = chartBalanceData;
      maxWinHits = 0;
      maxLossHits = 0;
      maxWinStake = 0;
      maxLossStake = 0;
      maxBalance = 0;
      $('#statMaxWinStake').html('0.00000000');
      $('#statMaxLossStake').html('0.00000000');
      $('#statMaxWinHits').html('0');
      $('#statMaxBalance').html('0.00000000');
      $('#statMaxLossHits').html('0');
      $('#statDiffBalance').html('0.00000000');
      isReseted = true;
    }
    if (resetTimer)
      clearTimeout(resetTimer);
    resetTimer = setTimeout(function () {
      elButtonResetBalance.removeClass('btn-danger btn-warning').addClass('btn-dark');
      $('#labelClickTrice').addClass('visually-hidden');
      if (isReseted)
        rfc.addClass('visually-hidden');
    }, 1000);
  }
  elButtonResetBalance.on('click', resetBalance);

  let elButtonStartAutoBet = $('#btPlayAutoBet');
  let elInputBetAmont = $('#betAmount');
  let elInputWinProfit = $('#inpWinProfit');
  let elLabelclickTwice = $('#labelClickTwice');
  let isSecondClick = false;
  let fWinProfit = 0;
  let statDepth = 33;
  let fRestrainer = 0.1;

  let secondCallTS = 0;
  function betAmountX2(evt, updateUI) {
    if (Date.now() - secondCallTS < 8)
      return;
    secondCallTS = Date.now();
    let vBetAmount = parseFloat(elInputBetAmont.val()) * (updateUI ? 1.0 : 2.0);
    let vBalance = parseFloat(elTheBalance.val());

    if (!isAutoBetRunning) {
      if (fRestrainer && vBetAmount > vBalance * fRestrainer) {
        if (!updateUI && !isSecondClick) {
          isSecondClick = true;
          elInputBetAmont.addClass('text-danger');
          elLabelclickTwice.removeClass('visually-hidden');
          setTimeout(function () {
            if (isSecondClick && vBetAmount <= vBalance * 0.5)
              elInputBetAmont.removeClass('text-danger');
            elLabelclickTwice.addClass('visually-hidden');
            isSecondClick = false;
          }, 500);
          return;
        }
      } else if (updateUI) {
        elInputBetAmont.removeClass('text-danger');
      }
      if (fRestrainer && vBetAmount > vBalance * 0.5) {
        elLabelclickTwice.find('span').removeClass('visually-hidden');
        if (!updateUI && (!isSecondClick || !evt.shiftKey))
          return;
        elInputBetAmont.addClass('text-danger fw-bold');
        /*setTimeout(function () {
          if (isSecondClick)
            ba.removeClass('text-danger');
          lct.addClass('visually-hidden');
          isSecondClick = false;
        }, 500);
        return;*/
      }
      isSecondClick = false;
      elLabelclickTwice.addClass('visually-hidden');
    } else {
      if ($('#selAutoX2script')[0].selectedIndex === 1) {
        if (fRestrainer && vBetAmount > vBalance * fRestrainer) {
          betAmountMin();
          return;
        }
      }
    }

    if (vBetAmount > vBalance) {
      vBetAmount = vBalance;
      elTheBalance.addClass('bg-danger');
      elInputBetAmont.addClass('bg-danger text-white').removeClass('text-danger');
      setTimeout(function () {
        elTheBalance.removeClass('bg-danger');
        elInputBetAmont.removeClass('bg-danger text-white').addClass('text-danger');
      },250);
    }
    elInputBetAmont.val(vBetAmount.toFixed(8));
    let profit = Math.floor((parseFloat($('#inpBetOdds').val())-1.0)*vBetAmount*1e8);
      elInputWinProfit.val((profit/1e8).toFixed(8));
    fWinProfit = parseFloat(elInputWinProfit.val());
  }
  function betAmountDiv2() {
    let vBetAmount = parseFloat(elInputBetAmont.val()) / 2.0;
    elInputBetAmont.val(vBetAmount.toFixed(8));
    betAmountX2(0, 1);
  }
  function betAmountDiv02() {
    if (Date.now() - secondCallTS < 8)
      return;
    secondCallTS = Date.now();
    let vBetAmount = parseFloat(elInputBetAmont.val()) * 0.16;
    var nearestExp2 = 2e-8;
    while (nearestExp2 < vBetAmount) nearestExp2 *= 2.0;
    vBetAmount = nearestExp2;
    elInputBetAmont.val(vBetAmount.toFixed(8));
    let profit = Math.floor((parseFloat($('#inpBetOdds').val())-1.0)*vBetAmount*1e8);
    elInputWinProfit.val((profit/1e8).toFixed(8));
    fWinProfit = parseFloat(elInputWinProfit.val());
  }
  function betAmountMin() {
    elInputBetAmont.val($('#inpSetMin').val());
    let v = parseFloat(elInputBetAmont.val());
    let profit = Math.floor((parseFloat($('#inpBetOdds').val())-1.0)*v*1e8);
    //if (!isAutoBetRunning)
      elInputWinProfit.val((profit / 1e8).toFixed(8));
    fWinProfit = parseFloat(elInputWinProfit.val());
    if (profit === 0)
      betAmountX2();
    if (!isAutoBetRunning) {
      elInputBetAmont.removeClass('text-danger fw-bold');
      elLabelclickTwice.find('span').addClass('visually-hidden');
    }
  }
  let lohi = {Lo: 0, Hi: 1, 0: 'Lo', 1: 'Hi'};
  let maxStatDepth = 4096;
  let statMaxGameBet = 0;
  let lohiClass = {Lo: 'text-primary', Hi: 'text-danger'};
  let winLosingClass = {true: 'text-success', false: 'text-danger'};
  let hits = 1;
  let maxWinHits = 0;
  let maxLossHits = 0;
  let maxWinStake = 0;
  let maxLossStake = 0;
  let fStatWagered = 0.0;
  let prevBetButton = null;
  let betCancelTimer = null;
  let maxBalance = 0;

  let $lblPressTwice = $('#labelPressTwice');
  let $inpBalanceChartWidth = $('#inpBalanceChartWidth');
  let showPressTwiceHint = false;
  let isBetReadyToPlay = true;
  let loseCtr = 0;
  function playBet(evt, target) {
    if (!isBetReadyToPlay) {
      return;
    }
    isBetReadyToPlay = false;
    let e = $(evt.target);
    if (!isAutoBetRunning && !e.hasClass('text-warning') && $('#cbBetTwiceMode')[0].checked) {
      if (prevBetButton)
        prevBetButton.removeClass('text-warning fw-bolder');
      prevBetButton = e.addClass('text-warning fw-bolder');

      showPressTwiceHint = true;
      betCancelTimer = setTimeout(function () {
        if (prevBetButton)
          prevBetButton.removeClass('text-warning fw-bolder');
        if (showPressTwiceHint) {
          $lblPressTwice.removeClass('visually-hidden');
          setTimeout(function () {
            $lblPressTwice.addClass('visually-hidden');
          }, 2000);
        }
      },3000);
      isBetReadyToPlay = true;
      return;
    }
    if (betCancelTimer)
      clearTimeout(betCancelTimer);
    showPressTwiceHint = false;
    e.removeClass('text-warning fw-bolder');
    $lblPressTwice.addClass('visually-hidden');
    prevBetButton = null;
    if (!target)
      target = e.attr('target');
    let vBetAmount = elInputBetAmont.val();
    let fBetAmount = parseFloat(vBetAmount);
    let randomBet = rand(loseCtr) > 0.5 ? 1 : 0;
    let isWin = lohi[target] === randomBet;
    if (isWin) {
      loseCtr = 0;
      if (fBetAmount > maxWinStake) {
        $('#statMaxWinStake').html(vBetAmount);
        maxWinStake = fBetAmount;
      }
      if (hits < 0) {
        if (hits < maxLossHits) {
          $('#statMaxLossHits').html(-hits);
          maxLossHits = hits;
        }
        statDiscrepancy[-hits]++;
        hits = 1;
      } else
        hits++;
    } else {
      loseCtr++;
      if (fBetAmount > maxLossStake) {
        $('#statMaxLossStake').html(vBetAmount);
        maxLossStake = fBetAmount;
      }
      if (hits > 0) {
        if (hits > maxWinHits) {
          $('#statMaxWinHits').html(hits);
          maxWinHits = hits;
        }
        statConcurrency[hits]++;
        hits = -1;
      } else
        hits--;
    }
    let isNewMaxBet = fBetAmount > statMaxGameBet;
    if (isNewMaxBet) {
      statMaxGameBet = fBetAmount;
      /*$('#textareaLog').val(
      'Maximal Game Bet: ' + statMaxGameBet.toFixed(8) +'\n'
          +$('#textareaLog').val()
      );*/
    }
    fStatWagered += fBetAmount;
    localStorage['wagered'] = fStatWagered;
    $('#statWagered').val(fStatWagered.toFixed(8));
    let profit = isWin ? fWinProfit : -fBetAmount;
    let fBalance = parseFloat(elTheBalance.val()) + profit;
    if (fBalance < 0.00000000) {
      if (isAutoBetRunning)
        playAutoBet();
      isBetReadyToPlay = true;
      return;
    }
    nonce++;
    var bcw = parseInt($inpBalanceChartWidth.val())*33;
    while (chartBalanceData.length > bcw)
      chartBalanceData.shift();
    chartBalanceData.push({x: nonce, y: fBalance});
    chartBalance.data = chartBalanceData;
    //chartBalance.addData({x: nonce, y: fBalance}, 1);
    let balance = fBalance.toFixed(8);
    let isNewMaxBalance = false;
    if (fBalance > maxBalance) {
      maxBalance = fBalance;
      $('#statMaxBalance').html(balance);
      isNewMaxBalance = true;
      series.stroke = am4core.color('green');
    } else
      if (fBalance < maxBalance*0.997)
        series.stroke = am4core.color('red');
    $('#statDiffBalance').html((maxBalance-fBalance).toFixed(8));
    let succfail = (isWin ? 'success':'danger');
    if (!isAutoBetRunning) {
      elTheBalance.removeClass('bg-success bg-danger').addClass('bg-' + succfail).val(balance);
      setTimeout(function () {elTheBalance.removeClass('bg-' + succfail);}, 500);
    } else
      elTheBalance.val(balance);
    let trs = elBetsTable.find('tr');
    if (firstVisibleTableTrIndex) {
      trs.hide();
      trs.slice(0, statDepth).show();
      firstVisibleTableTrIndex = 0;
    }
    if (trs.length >= maxStatDepth)
      trs.last().remove();
    if (trs.length >= statDepth)
      trs.eq(statDepth-1).hide();
    elBetsTable.prepend('<tr id="" class="table-'+ succfail +'">'+
      '<td>'+nonce+'</td>' +
      '<td class="'+(isNewMaxBet?'fw-bold':'')+'">'+elInputBetAmont.val()+'</td>' +
      '<td class="align-text-center '+lohiClass[target]+'">'+target+'</td>' +
      '<td class="'+lohiClass[lohi[randomBet]]+'">'+lohi[randomBet]+'</td>' +
      '<td>'+(isWin?'&nbsp;':'')+profit.toFixed(8)+'</td>' +
      '<td class="'+winLosingClass[isWin]+'">'+(Math.abs(hits) > 1?Math.abs(hits):'')+'</td>' +
      '<td class="'+(isNewMaxBet?'fw-bold':'')+(isNewMaxBalance?' text-success':'')+'">'+balance+'</td></tr>'
    );
    $('#tableRangeLabel').html(
      (firstVisibleTableTrIndex+1) +
      '..' + (firstVisibleTableTrIndex+statDepth) +
      ' of ' + (trs.length+1)
    );
    localStorage['balance'] = balance;
    localStorage['nonce'] = nonce;

    switch ($('#selAutoX2script')[0].selectedIndex) {
      case 1:
        if (isWin)
          betAmountMin();
        else
          betAmountX2();
        break;
    }

    if (isAutoBetRunning) {
      if (isAutoBetBullishMode) {
        if (isWin) {
          if (fBetAmount >= vMinimalProfitToBetReset)
            betAmountDiv02();
            //betAmountMin();
          else {
            betAmountX2();
          }
        } else
          if (fBetAmount*2.0 <= vMaximalAllowableBet) {
            betAmountX2();
          } else
            betAmountMin();
      }
      if (!isWin) {
        if (balance < balanceBeforeAutoBetStart*0.99) {
          elButtonStartAutoBet.click();
          isBetReadyToPlay = true;
          return;
        }
      }
    } else {
      fitInputWidth(0, elTheBalance);
      setTimeout(betAmountX2, 100, 0, 1);
    }
    isBetReadyToPlay = true;
    //console.log(target);
  }

  let isAutoBetRunning = false;
  let autoBetInterval = null;
  let throttlingDivider = 256;
  let balanceBeforeAutoBetStart = 0;
  function playAutoBet() {
    if (isAutoBetRunning) {
      clearInterval(autoBetInterval);
      isAutoBetRunning = false;
      elButtonStartAutoBet.removeClass('btn-warning')
      .addClass('btn-success')
      .find('span').html('Start Auto Bet');
      $('#labelClickTrice2').addClass('visually-hidden');
      if (!elhgt[0].checked)
        elhgt.click();
      setTimeout(betAmountMin, 250);
    }
    if (elButtonStartAutoBet.hasClass('btn-dark')) {
      elButtonStartAutoBet.removeClass('btn-dark').addClass('btn-danger');
      $('#labelClickTrice2').removeClass('visually-hidden');
    } else
    if (elButtonStartAutoBet.hasClass('btn-danger'))
      elButtonStartAutoBet.removeClass('btn-danger').addClass('btn-warning');
    else
    if (elButtonStartAutoBet.hasClass('btn-warning')) {
      isAutoBetRunning = true;
      elButtonStartAutoBet.find('span').html('Stop Auto Bet');
      let throttlingCtr = 0;
      isAutoBetBullishMode = $('#cbBullishAutoBet')[0].checked;
      balanceBeforeAutoBetStart = parseFloat(elTheBalance.val());
      //elBetsTable.hide();
      autoBetInterval = setInterval(function () {
        /*if (!(throttlingCtr%32)) {
          if (elButtonStartAutoBet.hasClass('btn-warning'))
            elButtonStartAutoBet.removeClass('btn-warning').addClass('btn-success');
          else
            elButtonStartAutoBet.removeClass('btn-success').addClass('btn-warning');
        }*/
        //TODO: auto balance multiplying script...
        if (!(throttlingCtr%throttlingDivider)) {
          /*if (isBetReadyToPlay) {
            let lohi = rand() > 0.5;
            $('#' + (lohi ? 'btPlayBetLo' : 'btPlayBetHi')).click();
          }*/
          if (isBetReadyToPlay && !preventPlayBet)
            playBet(0, Math.random() > 0.5 ? 'Hi' : 'Lo');
        }
        throttlingCtr++;
      }, 4);
      return;
    }
    if (resetTimer)
      clearTimeout(resetTimer);
    resetTimer = setTimeout(function () {
      if (!isAutoBetRunning)
        elButtonStartAutoBet.removeClass('btn-danger btn-warning btn-success').addClass('btn-dark');
      $('#labelClickTrice2').addClass('visually-hidden');
    }, 1000);
  }
  elButtonStartAutoBet.on('click', playAutoBet);

  function changeGameLogDepth() {
    statDepth = parseInt($(this).val());
    let trs = elBetsTable.find('tr');
    trs.each(function (i,e) {
      $(e)[i<statDepth ? 'show' : 'hide']();
    });
    $('#tableRangeLabel').html(
    (firstVisibleTableTrIndex+1) +
    '..' + (firstVisibleTableTrIndex+statDepth) +
    ' of ' + trs.length
    );
  }
  $('#inpStatDepthRange')
    .css('max-width', ($('#gameLogTable').width()-$('#gameLogHeader h1.h5').width()-40) + 'px')
    .on('input', changeGameLogDepth);
  let firstVisibleTableTrIndex = 0;
  elBetsTable.on('mousewheel', function (evt) {
    let trs = elBetsTable.find('tr');
    if (evt.originalEvent.deltaY > 0) {
      if (firstVisibleTableTrIndex+statDepth < trs.length) {
        trs.eq(firstVisibleTableTrIndex).hide();
        trs.eq(firstVisibleTableTrIndex + statDepth).show();
        firstVisibleTableTrIndex++;
      }
    } else {
      if (firstVisibleTableTrIndex) {
        trs.eq(--firstVisibleTableTrIndex).show();
        trs.eq(firstVisibleTableTrIndex + statDepth).hide();
      }
    }
    $('#tableRangeLabel').html(
      (firstVisibleTableTrIndex+1) +
      '..' + (firstVisibleTableTrIndex+statDepth) +
      ' of ' + trs.length
    );
    //console.log(evt)
  });

  let elhgt = $('#cbHideGameTable');
  let prevAutoBetSpeed = 0;
  function changeThrottlingDivider() {
    let v = parseInt($(this).val());
    switch (v) {
      case 0: throttlingDivider = Number.MAX_SAFE_INTEGER; break;
      case 1: throttlingDivider = 256; break;
      case 2: throttlingDivider = 128; break;
      case 3: throttlingDivider = 64; break;
      case 4: throttlingDivider = 32; break;
      case 5: throttlingDivider = 16; break;
      case 6: throttlingDivider = 8; break;
      case 7: throttlingDivider = 4; break;
      case 8: throttlingDivider = 2; break;
      case 9: throttlingDivider = 1; break;
    }
    if (v === 5 && prevAutoBetSpeed === 4 && elhgt[0].checked)
      elhgt.click();
    else
    if (v === 3 && prevAutoBetSpeed === 4 && !elhgt[0].checked)
      elhgt.click();
    prevAutoBetSpeed = v;
  }
  $('#inpAutoBetSpeed').on('change', changeThrottlingDivider);

  function changeAutoBetSmartMode() {
    isAutoBetBullishMode = $(this)[0].checked;
    $('#maxAllowableBetControlsContainer')[isAutoBetBullishMode?'removeClass':'addClass']('visually-hidden');
  }
  $('#cbBullishAutoBet').on('change', changeAutoBetSmartMode);

  elhgt.on('change', function () {
    elBetsTable[$(this)[0].checked ? 'show' : 'hide']();
  });

  let elInputMinProfitToBetReset = $('#minProfitToBetReset');
  if (localStorage['minimalProfitToBetReset'])
    elInputMinProfitToBetReset.val(localStorage['minimalProfitToBetReset']);
  vMinimalProfitToBetReset = parseFloat(elInputMinProfitToBetReset.val());
  function changeMinProfitToBetReset() {
    let newval = parseFloat($(this).val());
    if (newval > vMinimalProfitToBetReset) {
      if (vMinimalProfitToBetReset === 0)
        vMinimalProfitToBetReset = 1e-8;
      if (vMinimalProfitToBetReset*2.0 < parseFloat(elTheBalance.val()))
        vMinimalProfitToBetReset *= 2.0;
      else {
        elTheBalance.addClass('bg-danger');
        elInputMinProfitToBetReset.addClass('bg-danger text-white');
        setTimeout(function () {
          elTheBalance.removeClass('bg-danger');
          elInputMinProfitToBetReset.removeClass('bg-danger text-white');
        },250);
      }
    } else {
      vMinimalProfitToBetReset /= 2.0;
      if (vMinimalProfitToBetReset === 1e-8)
        vMinimalProfitToBetReset = 0;
    }
    elInputMinProfitToBetReset.val((Math.floor(vMinimalProfitToBetReset*1e8)/1e8).toFixed(8));
    vMinimalProfitToBetReset = parseFloat(elInputMinProfitToBetReset.val());
    localStorage['minimalProfitToBetReset'] = elInputMinProfitToBetReset.val();
  }
  elInputMinProfitToBetReset.on('change', changeMinProfitToBetReset);

  let elInputMaxAllowableBet = $('#maxAllowableBet');
  if (localStorage['maximalAllowedBet'])
    elInputMaxAllowableBet.val(localStorage['maximalAllowedBet']);
  vMaximalAllowableBet = parseFloat(elInputMaxAllowableBet.val());
  function changeMaxAllowableBet() {
    let newval = parseFloat($(this).val());
    if (newval > vMaximalAllowableBet) {
      if (vMaximalAllowableBet === 0)
        vMaximalAllowableBet = 1e-8;
      if (vMaximalAllowableBet*2.0 < parseFloat(elTheBalance.val()))
        vMaximalAllowableBet *= 2.0;
      else {
        elTheBalance.addClass('bg-danger');
        elInputMaxAllowableBet.addClass('bg-danger text-white');
        setTimeout(function () {
          elTheBalance.removeClass('bg-danger');
          elInputMaxAllowableBet.removeClass('bg-danger text-white');
        },250);
      }
    } else {
      vMaximalAllowableBet /= 2.0;
      if (vMaximalAllowableBet === 1e-8)
        vMaximalAllowableBet = 0;
    }
    elInputMaxAllowableBet.val((Math.floor(vMaximalAllowableBet*1e8)/1e8).toFixed(8));
    vMaximalAllowableBet = parseFloat(elInputMaxAllowableBet.val());
    localStorage['maximalAllowedBet'] = elInputMaxAllowableBet.val();
  }
  elInputMaxAllowableBet.on('change', changeMaxAllowableBet);

  let elInpSetMin = $('#inpSetMin');
  if (localStorage['setMinValue'])
    elInpSetMin.val(localStorage['setMinValue']);
  let vSetMinValue = parseFloat(elInpSetMin.val());
  function changeSetMinValue() {
    let newval = parseFloat($(this).val());
    if (newval > vSetMinValue) {
      if (vSetMinValue === 0)
        vSetMinValue = 1e-8;
      if (vSetMinValue*2.0 < parseFloat(elTheBalance.val()))
        vSetMinValue *= 2.0;
      else {
        elTheBalance.addClass('bg-danger');
        elInpSetMin.addClass('bg-danger text-white');
        setTimeout(function () {
          elTheBalance.removeClass('bg-danger');
          elInpSetMin.removeClass('bg-danger text-white');
        },250);
      }
    } else {
      vSetMinValue /= 2.0;
      if (vSetMinValue === 1e-8)
        vSetMinValue = 0;
    }
    elInpSetMin.val((Math.floor(vSetMinValue*1e8)/1e8).toFixed(8));
    vSetMinValue = parseFloat(elInpSetMin.val());
    localStorage['setMinValue'] = elInpSetMin.val();
  }
  elInpSetMin.on('change', changeSetMinValue);

  function changeX2Restrainer() {
    let val = $(this).val();
    localStorage['x2RestrainerPercent'] = val;
    fRestrainer = parseFloat(val)/100.0;
  }
  var x2rp = localStorage['x2RestrainerPercent'];
  fRestrainer = x2rp ? parseFloat(x2rp)/100.0 : 0.1;
  $('#inpRestrainX2').on('change', changeX2Restrainer).val(x2rp ? x2rp : 10);

  $('#selAutoX2script').on('change', function () {
    switch (this.selectedIndex) {
      case 0: $(this).removeClass('bg-warning'); break;
      default: $(this).addClass('bg-warning'); break;
    }
  });

  let irf = $('#inpResetFactor');
  if (localStorage['resetFactor'])
    irf.val(localStorage['resetFactor']);
  irf.on('change', function () {
    localStorage['resetFactor'] = $(this).val();
  });


  elTheBalance.on('keyup', fitInputWidth).on('change', fitInputWidth);
  $('#btBetAmountX2').on('click', betAmountX2);
  $('#btBetAmountDiv2').on('click', betAmountDiv2);
  $('#btBetAmountMin').on('click', betAmountMin);
  let bpbl = $('#btPlayBetLo').on('click', playBet);
  let bpbh = $('#btPlayBetHi').on('click', playBet);

  $(window).on('keyup', function (evt) {
    if (isAutoBetRunning || document.activeElement.tagName.toLowerCase() === 'input')
      return;
    if (!evt['code'])
      evt.code = evt.originalEvent.code;
    switch (evt.code) {
      case 'KeyS': case 'ArrowUp': betAmountX2(evt); break;
      case 'KeyF': betAmountDiv2(evt); break;
      case 'KeyD': case 'ArrowDown': betAmountMin(evt); break;
      case 'ArrowLeft': playBet({target: bpbl[0]}, 'Lo'); break;
      case 'ArrowRight': playBet({target: bpbh[0]}, 'Hi'); break;
    }
    //console.log(document.activeElement.tagName);
  });
  betAmountMin();
  if (localStorage['balance'])
    elTheBalance.val(localStorage['balance']);
  if (localStorage['nonce'])
    nonce = parseInt(localStorage['nonce']);
  if (localStorage['wagered'])
    fStatWagered = parseFloat(localStorage['wagered']);
  $('#statWagered').val(fStatWagered.toFixed(8));

  var bcw = parseInt($inpBalanceChartWidth.val())*33;
  var y = parseFloat(elTheBalance.val());
  for (var i=1; i<=bcw; i++) {
    chartBalanceData.push({x: nonce-bcw+i, y: y});
    //y += 0.00001000;
  }
  //var scale = d3.scale.linear().domain([Math.floor(y*1e8), Math.floor(y*2e8)]).nice();
/*    chartBalance = new Rickshaw.Graph({
    element: $('#balanceChart')[0],
    width: 333,
    height: 200,
    renderer: 'line',
    interpolation: 'step',
    offset: 'zero',
    stroke: true,
    preserve: true,
    series: [{
      scale: scale,
      color: 'steelblue',
      data: chartBalanceData
    }]
  });
  chartBalance.render();

  var ay = new Rickshaw.Graph.Axis.Y.Scaled({
    graph: chartBalance,
    orientation: 'right',
    scale: scale,
    //tickValues: [y-0.1, y+0.1],
    tickFormat: function (y) {return (y/1e8).toFixed(8)}
  });
  ay.render();
*/
  //am4core.useTheme(am4themes_animated);
  am4core.addLicense('ch-custom-attribution');
  var chartBalance = am4core.create('balanceChart', am4charts.XYChart);
  chartBalance.data = chartBalanceData;
  var axisX = chartBalance.xAxes.push(new am4charts.CategoryAxis());
  axisX.fontSize = 10;
  axisX.strictMinMax = true;
  axisX.renderer.minGridDistance = 50;
  //axisX.numberFormatter.numberFormat = '#.0000';
  axisX.dataFields.category = 'x';
  //axisX.tooltip.disabled = true;
  //axisX.width = am4core.percent(100);
  var axisY = chartBalance.yAxes.push(new am4charts.ValueAxis());
  axisY.fontSize = 10;
  axisY.strictMinMax = true;
  axisY.renderer.minGridDistance = 20;
  //axisY.numberFormatter.numberFormat = '#.##';
  var series = chartBalance.series.push(new am4charts.LineSeries());
  series.dataFields.categoryX = 'x';
  series.dataFields.valueY = 'y';
  series.yAxes = axisY;
  series.xAxes = axisX;
  series.tooltipText = "{valueY.formatNumber('#.00000000')}";
  series.stroke = am4core.color('steelblue');

  chartBalance.cursor = new am4charts.XYCursor();
  //chartBalance.cursor.xAxes = axisX;
  //chartBalance.cursor.yAxes = axisY;

  // some UI corrections
  let preventPlayBet = false;
  document.onmousedown = function () {preventPlayBet = true};
  document.onmouseup = function () {preventPlayBet = false};

}
